import {store} from "../app/store";
import {logoutAction} from "../core/auth/authSlice";

const status401Interceptor = (err: any) => {
    console.log('attivare procedura logout');
    // Auth.logout();
    store.dispatch(logoutAction());
    return Promise.reject(err)
}

export default status401Interceptor;
