import React, {useEffect} from 'react';
import './App.scss';
import {BrowserRouter as Router} from 'react-router-dom'
import AuthRoute from "./core/routes/AuthRoute";
import PublicRoute from "./core/routes/PublicRoute";
import http from "./shared/utils/http";
import {useAppDispatch, useAppSelector} from "./app/hooks";
import {logoutAction, resetUser, selectAuth, selectUser, setAuth, setUser} from "./core/auth/authSlice";

let init = false;
function App() {
    const auth = useAppSelector(selectAuth)
    const user = useAppSelector(selectUser)
    const dispatch = useAppDispatch();
    // const [auth, setAuth] = useState<boolean | undefined>();
    // const [user, setUser] = useState<UserModel>();
    const autoLogin = () => {
        http.get('users/logged').then(res => {
            dispatch(setAuth(true))
            // setAuth(true)
            dispatch(setUser(res))
            // setUser(res)
        })
    }

    useEffect(() => {
        if(!init){
            init= true;
            // Auth.logout = logoutHandler;
            // Auth.loginAction = loginAction;
            localStorage.getItem('token') ? autoLogin() : dispatch(setAuth(false));
        }
    }, [])

    const logoutHandler = () => {
        dispatch(logoutAction());
        // dispatch(setAuth(false));
        // dispatch(resetUser());
        // localStorage.removeItem('token');
    }
    return (
        <Router>
            <div className="container">
                {
                    auth === undefined ?
                        <div>Caricamento in corso</div> :
                        <>
                            {auth ?
                                <AuthRoute logout={logoutHandler} user={user} /> :
                                <PublicRoute />}
                        </>
                }

            </div>
        </Router>

    );


}

export default App;
