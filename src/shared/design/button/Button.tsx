

function Button(props: any){
    let type: 'button' | 'submit' | 'reset' = props.type || 'button';
    let classList = ['button-component', 'btn', (props.classBt || 'btn-primary')];
    let text = props.text || 'Salva';
    const clickHandler = () => {
        props.clickAction && props.clickAction()
    }
    return <button
                type={type}
                onClick={clickHandler}
                className={classList.join(' ')}>
                    {props.children || text}
            </button>;
}
export default Button
