import {ValueOf} from "../../utils/typescript/ValueOf";

export const classListTableData = {
    alternativeRow: 'alternative-row',
    test: 'test'
} as const;
// type classTest = {
//     alternativeRow: string;
//     test: string
// }
// export type classListTableDataType = typeof classListTableData;

// type classTest = {
//   readonly  alternativeRow: 'alternating-row';
//   readonly  test: 'test'
// }
// export type classListTableDataKeys = keyof classListTableDataType;
// 'alternativeRow' | 'test'

// export type classListTable = classListTableDataType['alternativeRow'] | classListTableDataType['test']
// export type classListTable = classListTableDataType[classListTableDataKeys]
 export type classListTable = ValueOf<typeof classListTableData>

//'alternating-row' | 'test'
// Obiettivo finale 'alternating-row' | 'test'


// export type classListTable = classListTableDataType[keyof classListTableDataType]
