import React from "react";
import {BootstrapType} from "../model/BootstrapType";

interface CardCustomProps{
    title: string;
    children: React.ReactNode;
    classList?: string[];
    headerClass?: BootstrapType[];
    bodyClass?: BootstrapType[];
}
const CardCustom: React.FC<CardCustomProps> = ({
                                                   title,
                                                   classList = [],
                                                   headerClass = [],
                                                   bodyClass= [],
                                                   children}) => {
    const listClass = ['card', ...classList];
    const listHeader = ['card-header', ...headerClass];
    const listBody = ['card-body', ...bodyClass];
    return (
        <div className={listClass.join(' ')}>
            <div className={listHeader.join(' ')}>
                {title}
            </div>
            <div className={listBody.join(' ')}>
                {children}
            </div>
        </div>
    )
}

export default CardCustom;
