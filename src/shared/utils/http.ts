import axios, {AxiosRequestConfig, AxiosResponse} from "axios";

const urlBase = process.env.REACT_APP_URL;
//http://maps.api
//login  => http://80.211.68.52/api/login
const getUrl = (url: string) => {
    return url.startsWith('http') ? url : `${urlBase}${url}`
}

const getConfig = (config: AxiosRequestConfig = {}) => {
    const token = localStorage.getItem('token');
    return {
        ...config,
        config: 'test',
        headers: {
            ...(config.headers || {}),
            ...(token? {Authorization: token} : {})
        }
    }
}

export interface HttpCall {
    get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;
    delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>,
    post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>,
    put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>,
    patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R>,
}

export interface HttpCallData {
    get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>,
    post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>,
    put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>,
    patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>,
}


export const httpBase: HttpCall = {
    get: (url, config) => axios.get(getUrl(url), getConfig(config)),
    post: (url, data, config) => axios.post(getUrl(url), data, getConfig(config)),
    put: (url, data, config) => axios.put(getUrl(url), data, getConfig(config)),
    patch: (url, data, config) => axios.patch(getUrl(url), data, getConfig(config)),
    delete: (url, config) => axios.delete(getUrl(url), getConfig(config))
}



export const http: HttpCallData = {
    get: (url, config) => httpBase.get(url, config).then(res => res.data),
    post: (url, data, config) => httpBase.post(url, data, config).then(res => res.data),
    put: (url, data, config) => httpBase.put(url, data, config).then(res => res.data),
    patch: (url, data, config) => httpBase.patch(url, data, config).then(res => res.data),
    delete: (url, config) => httpBase.delete(url, config).then(res => res.data)
}


export default http;

