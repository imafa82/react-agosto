import {useState} from "react";

export function useSearch(data: any[], properties: string[] = []){
    const [search, setSearch] = useState<string>('');
    // 'Bret'
    // properties =  ['name', 'username', 'email']
    // email acc true
    const filterData = data.filter(ele => (properties.length? properties : Object.keys(ele)).reduce((acc: boolean, property: string) => {
            return ele[property].toString().toLocaleLowerCase().trim().includes(search.toLocaleLowerCase().trim()) || acc
        }, false)
    )
    const changeHandler = (event: any) => {
        setSearch(event.target.value)
    }
    return {
        search,
        changeHandler,
        filterData
    }
}
