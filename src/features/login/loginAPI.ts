import {http} from "../../shared/utils/http";
import {UserModel} from "../../model/User.model";

export const loginCall = (userData: any) => http.post<{token: string; user: UserModel}>(`login`, userData,
    {headers: {
        'Content-Type': 'application/json'
        }})
