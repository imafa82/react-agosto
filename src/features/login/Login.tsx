import React, {FormEventHandler, useState} from "react";
import { useNavigate } from "react-router-dom";
import {loginCall} from "./loginAPI";
import {setAuth, setUser} from "../../core/auth/authSlice";
import {useAppDispatch} from "../../app/hooks";
interface UserLogin{
    email?: string;
    password?: string
}

const Login: React.FC = () => {
    const [userData, setUserData] = useState<UserLogin>({});
    const navigate = useNavigate()
    const dispatch = useAppDispatch()
    const loginHandler: FormEventHandler<HTMLFormElement> = (event) => {
        event.preventDefault();
        loginCall(userData).then(res => {
            localStorage.setItem('token', res.token)
            dispatch(setAuth(true))
            dispatch(setUser(res.user))
            // Auth.loginAction && Auth.loginAction(res.user)
            navigate('/users')
        }, err => {
                console.log(err);
        })


    }
    const changeData: React.ChangeEventHandler<HTMLInputElement> = (event) => {
       setUserData({...userData, [event.target.name]: event.target.value})
    }

    return (<div>
        <h1>Login</h1>
        <form onSubmit={loginHandler}>
            <div className="form-group">
                <input type="email" className="form-control" name="email" onChange={changeData} value={userData.email || ''}/>
            </div>
            <div className="form-group">
                <input type="password" className="form-control" name="password" onChange={changeData} value={userData.password  || ''}/>
            </div>
            <button className="btn btn-primary">Login</button>
        </form>

    </div>)
}

export default Login;
