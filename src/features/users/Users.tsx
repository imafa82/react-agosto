import React, {useCallback, useState} from "react";
import {UserApi} from "../../model/UserApi";
import TableCustom from "../../shared/design/table/TableCustom";
import {classListTableData} from "../../shared/design/table/TableModel";
import {useSearch} from "../../shared/utils/useSearch";
import {useUsers} from "./useUsers";
import TotUser from "./TotUser";

const Users: React.FC = () => {
    const [display, setDisplay] = useState(false);
    const {users, columns, deleteUser} = useUsers()
    const {search, changeHandler, filterData} = useSearch(users, ['name', 'username'])

    const templates = {
        actions: (value : any, row: UserApi) => {
            return <i className="fa fa-trash" onClick={() => deleteUser(row.id)}></i>
        },
        username: (value: string) => {
            return value.toLocaleUpperCase();
        }
    }



    const test = useCallback(() => {
        console.log('ccc')
    }, [])
    return (<>
        <button onClick={() => setDisplay(true)} >Mostra </button>
        <input value={search} onChange={changeHandler} />
        <TableCustom
            classList={[classListTableData.alternativeRow]}
            columns={columns}
            data={filterData}
            templates={templates} />
        <TotUser tot={filterData.length} test={test} />
    </>)
}

export default Users;
