import {useEffect, useState} from "react";
import {UserApi} from "../../model/UserApi";
import {columnsUserTable} from "./usersData";

export function useUsers(){
    const [users, setUsers] = useState<UserApi[]>([])
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(res => setUsers(res))
    }, [])

    const columns = columnsUserTable;
    const deleteUser = (id: any) => {
        fetch('https://jsonplaceholder.typicode.com/users/'+id, {method: 'DELETE'}).then(res => {
            setUsers(users.filter(user => user.id !== id))
        })
    }
    return {
        users,
        columns,
        deleteUser
    }
}
