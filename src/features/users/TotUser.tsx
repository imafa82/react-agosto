import React, {useEffect, useState} from "react";

const TotUser: React.FC<{tot: number, test: () => void}> = ({tot, test}) => {
    console.log('passo da qui')
    const [display, setDiplay] = useState(true);
    useEffect(() => {
        test && test()
    }, [display])
    return (
        <div>
            {display && tot}
            <button onClick={() => setDiplay(!display)} >Test</button>
        </div>
    )
}

export default React.memo(TotUser)
