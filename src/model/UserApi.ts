export interface UserApi{
    name: string;
    id: number;
    username: string;
    email: string;
}

//keyof userApi
// 'name' | 'id' | 'username' | 'email'
