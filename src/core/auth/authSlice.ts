import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
import {UserModel} from "../../model/User.model";
import {incrementByAmount, selectCount} from "../../features/counter/counterSlice";


export interface AuthState {
  auth?: boolean;
  user?: UserModel;
}

const initialState: AuthState = {};


export const authSlice = createSlice({
  name: 'auth',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setUser: (state, action: PayloadAction<UserModel>) => {
      state.user = action.payload;
    },
    resetUser: (state) => {
      state.user = undefined;
    },
    setAuth: (state, action: PayloadAction<boolean>) => {
      state.auth = action.payload;
    }
  },
});


export const logoutAction =
    (): AppThunk =>
        (dispatch) => {
          localStorage.removeItem('token');
          dispatch(setAuth(false));
          dispatch(resetUser());
        };


export const { setUser, setAuth,  resetUser} = authSlice.actions;

export const selectUser = (state: RootState) => state.auth.user;
export const selectAuth = (state: RootState) => state.auth.auth;


export default authSlice.reducer;
