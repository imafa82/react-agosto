import {NavLink} from "react-router-dom";
import React from "react";
import MenuLeft from "./MenuLeft";
import MenuRight from "./MenuRight";

const HeaderLayout: React.FC<{menuLeft: React.ReactNode, menuRight: React.ReactNode}> = ({menuLeft, menuRight}) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"    data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand" href="#">Navbar</a>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    {menuLeft}
                    {menuRight}
                </div>
            </div>
        </nav>
    )
}

export default HeaderLayout;
