import React from "react";
import { NavLink } from "react-router-dom";
import {UserModel} from "../../model/User.model";
import MenuLeft from "./MenuLeft";
import MenuRight from "./MenuRight";
import HeaderLayout from "./HeaderLayout";

const Header: React.FC<{logout: () => void, user?: UserModel}> = ({logout, user}) => {
    return (
        <HeaderLayout menuLeft={<MenuLeft />} menuRight={<MenuRight user={user} logout={logout} />} />
        )

}

export default Header;
