import {NavLink} from "react-router-dom";
import React from "react";
import {UserModel} from "../../model/User.model";

const MenuRight: React.FC<{user?: UserModel, logout: () => void}> = ({user, logout}) => {
    return (
        <>
            {user && <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        {user.name} {user.surname}
                    </a>
                    <ul className="dropdown-menu dropdown-menu-end" aria-labelledby="navbarScrollingDropdown">
                        <li><a className="dropdown-item" href="#" onClick={logout}>Logout</a></li>
                    </ul>
                </li>
            </ul>}
        </>
    )
}

export default MenuRight;
