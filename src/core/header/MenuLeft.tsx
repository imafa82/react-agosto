import {NavLink} from "react-router-dom";
import React from "react";
import authList from "../routes/authList";

const MenuLeft: React.FC = () => {

    return (
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {authList.list.filter(route => route.title).map(route => {
                return (<li key={route.path} className="nav-item">
                    <NavLink to={route.path}
                             className={({isActive}) => isActive? 'nav-link text-info' : 'nav-link'}
                    >{route.title}</NavLink>
                </li>)
            })}
        </ul>
    )
}

export default MenuLeft;
