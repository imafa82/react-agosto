import React from "react";

export interface RoutesModel{
    list: {
        path: string;
        component: React.ReactComponentElement<any>,
        name: string;
        permissions?: string[];
        title?: string
    }[];
    redirect: string
}
