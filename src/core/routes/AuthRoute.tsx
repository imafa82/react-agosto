import React from "react";
import {Navigate, Route, Routes} from "react-router-dom";
import Header from "../header/Header";
import authList from "./authList";
import {UserModel} from "../../model/User.model";
import publicList from "./publicList";

const AuthRoute: React.FC<{logout: () => void, user?: UserModel}> = ({logout, user}) => {

    return (<>
        <Header logout={logout} user={user}  />
        <Routes>
            {authList.list.map(route => <Route
                                            key={route.path}
                                            path={route.path}
                                            element={route.component}
                                        />)}
            {authList.redirect && <Route path="*" element={<Navigate to={authList.redirect} />} />}
        </Routes>
    </>)
}

export default AuthRoute;
