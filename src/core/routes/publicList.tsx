import {RoutesModel} from "./models/Routes.model";
import Login from "../../features/login/Login";

const publicList: RoutesModel = {
    list: [
        {
            path: '/',
            name: 'login',
            component: <Login />
        }
    ],
    redirect: '/'
}

export default publicList;
