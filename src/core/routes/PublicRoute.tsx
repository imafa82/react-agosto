import React from "react";
import {Navigate, Route, Routes} from "react-router-dom";
import publicList from "./publicList";

const PublicRoute: React.FC = () => {

    return (<>
        <Routes>
            {publicList.list.map(route => <Route
                                            path={route.path}
                                            element={route.component}/>)}
            {publicList.redirect && <Route path="*" element={<Navigate to={publicList.redirect} />} />}
        </Routes>
    </>)
}

export default PublicRoute;
