import {RoutesModel} from "./models/Routes.model";
import Users from "../../features/users/Users";
import Actors from "../../features/actors/Actors";

const authList: RoutesModel = {
    list: [
        {
            path: '/users',
            name: 'users.index',
            component: <Users />,
            title: 'Utenti'
        },
        {
            path: '/actors',
            name: 'actors.index',
            component: <Actors />,
            title: 'Attori'
        },
        {
            path: '/attori',
            name: 'actors.index',
            component: <Actors />
        }
    ],
    redirect: '/users'
}

export default authList;
